Latex template for master and PhD theses at the University of Applied Sciences
Bonn-Rhein-Sieg.

Currently, the template is for the Electrical Engineering, Mechanical
Engineering and Technical Journalism, but can easily be modified for use in
other departments.

Students should consult their primary examiner for how the thesis should
specifically be formatted.

Note that if your thesis is with a company that has protected intellectual
property, you must include a "Sperrvermerk" (blocking or barring notice) on the thesis.

If you have a compnay logo that you want to include, then uncomment the
following line and adjust the height/width accordingly:
\includegraphics[height=1.2cm]{second_logo.pdf}

If you are using Linux, the included Makefile will easily compile the Latex
document. You must change the line that specifies which version (German or
English) you are compiling (i.e. "NAME    = thesis_deutsch"). Then use
the following two commands:
make clean
make

The original template and Makefile were created by Dr. Rudolf Berrendorf (http://berrendorf.inf.h-brs.de/index_e.html), and updated by Karl N. Kirschner.

University of Applied Sciences Bonn-Rhein-Sieg
Grantham-Allee 20
53757 Sankt Augustin - Germany
Phone +49 2241 865 0
https://www.h-brs.de/en

