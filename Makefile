############################################################
#
# Makefile f�r Abschlussarbeit
#
# M�gliche Aufrufe:
#  - make        : erzeugen der gesamten Arbeit als pdf
#  - make clean  : l�schen fast aller Dateien im Verzeichnis
#
# M�gliche/n�tige Anpassungen gekennzeichnit mit ???
#
############################################################

# ??? Name des LaTeX-Datei mit Endung .tex (ggfs. anpassen)
NAME	= thesis_deutsch

# �berschreiben Default-Regel f�r .tex Endungen
%.dvi: %.tex

# Regel, um aus .bib eine .bbl zu machen
%.bbl: %.bib
	bibtex $*

# Verhindert das L�schen von Zwischendateien nach make
.SECONDARY: $(NAME).bbl $(NAME).dvi

.PHONY: default clean


############################################################
#
# ??? LaTeX kennt entweder LaTeX+dvips oder pdflatex
# Unterschiede selber rausfinden (z.B.- unterst�tzte Grafikformate)
# Eine Version davon muss man sich aussuchen.
#
############################################################

# Version 1 (LaTeX + dvips; hier auskommentiert)
#TEX	= latex
#DVIPDF	= dvipdf
# aus tex-Datei pdf-datei generieren inkl. aktuellem Literaturverzeichnis
#%.pdf: %.tex
#	$(TEX) $*
#	-bibtex $*
#	$(TEX) $*
#	$(TEX) $*
#	$(DVIPDF) $*

# Version 2 (pdflatex; derzeit auskommentiert)
TEX	= pdflatex
# aus tex-Datei pdf-datei generieren
%.pdf: %.tex
	$(TEX) $*
	-bibtex $*
	$(TEX) $*
	$(TEX) $*

############################################################
# das sind die m�glich Ansprungziele beim Aufruf

default:: $(NAME).pdf

clean::
	-rm -f $(NAME).pdf $(NAME).aux $(NAME).toc $(NAME).dvi $(NAME).ind $(NAME).ilg \
        $(NAME).cb $(NAME).cb2 $(NAME).idx $(NAME).log $(NAME).bbl $(NAME).blg \
        $(NAME).out $(NAME).lof $(NAME).lot $(NAME).nav $(NAME).snm $(NAME).vrb
############################################################


